;(function () {

angular.module("simpleConfigurator", [])
    .provider("configuration", ConfigurationProvider)

function ConfigurationProvider() {
    var configuration;

    this.setConfiguration = function (config) {
        configuration = config;
    };

    this.$get = function () {
        return configuration;
    };
}

})();
